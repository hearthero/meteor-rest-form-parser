Package.describe({
  name: 'hearthero:rest-form-parser',
  version: '1.0.0',

  // Brief, one-line summary of the package.
  summary: 'Parse multipart/form-data from request.',

  // URL to the Git repository containing the source code for this package.
  git: 'https://gitlab.com/hearthero/meteor-rest-form-parser',

  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md',
});

Package.onUse(function (api) {
  api.versionsFrom('1.0');
  api.use('simple:json-routes@2.1.0');
  api.addFiles('form_parser.js', 'server');
});

Npm.depends({
  busboy: '0.3.1'
});
