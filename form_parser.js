/**
 * Parses multipart/form-data from the incoming request
 *
 * Each field gets attached to the request body
 * A file input adds the fileInfo to the request as well as a file promise
 * when this promise fulfills it returns a complete buffer.
 * The promise fails if reading takes 10 seconds or more
 *
 * @middleware
 */
 
import Busboy from 'busboy';

JsonRoutes.Middleware.parseForm = function (req, res, next) {

    const busboy = new Busboy({ headers: req.headers });
    const buffer = [];

    const fileInfo = {
        size: 0,
        magic: null,
        mime: ''
    };

    busboy.on('field', (field, value) => {
        req.body[field] = value;
    });

    busboy.on('file', (field, file, filename, encoding, mime) => {
        fileInfo.name = filename;
        fileInfo.mime = mime;

        file.on('data', (data) => {
            fileInfo.size += data.length;
            if (fileInfo.magic === '') {
                fileInfo.magic = data.slice(0,4);
            }
            buffer.push(data);
        });
        req.file = new Promise((resolve, error) => {
            file.on('end', () => {
                resolve(Buffer.concat(buffer));
            });
            setTimeout(() => {
                error('Timeout');
            }, 10000);
        });
    });

    req.fileInfo = fileInfo;
    req.pipe(busboy);
    next();
}
