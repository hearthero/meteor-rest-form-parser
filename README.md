# hearthero:rest-form-parser

SimpleRest middleware for parsing multipart/form-data from a HTTP request

### Middleware Name

This middleware can be accessed as: 

**`JsonRoutes.Middleware.parseForm`**

### Request Properties Required

- None

### Request Properties Modified

- `request.fileInfo`
  - _Object_
  - Contains `size`, `magic` and `mime` of the uploaded file

- `request.file`
  - _Promise\<Buffer\>_
  - When the promise fulfills it returns the complete buffer otherwise it will return an error when reading the file takes more than 10 seconds

- `request.body`
  - _Object_
  - Each form field gets added to the `body`

## Usage

There is a multitude of scenarios where parsing form data of a request can be useful. The most common would be the upload of files by REST API.

Here is an example of combining the form parser with meteor-files `write()` method:

```js
// Here we are using the magic bytes to specifically look for certain files.
// Alternatively this can also be done with the mime type for more common files.
const magic = req.fileInfo.magic.toString('hex');

switch (magic) {
    case '50415443': // IPS  
    case '55505331': // UPS  
    case '42505331': // BPS  
    case 'd6c3c400': // xDelta  
        break;
    default:
        const error = new Meteor.Error('not-acceptable', 'The uploaded file is not a valid patch.');
        error.statusCode = 406;
        throw error;
}

req.file.then(
    (data) => {
        Files.write(data, {
            fileName: req.thisFile.name,
            type: req.thisFile.mime,
        }, (error, fileRef) => {
            if (error) {
                const httpError = new Meteor.Error('internal-server-error', error);
                httpError.statusCode = 500;
                throw httpError;
            } else {
                JsonRoutes.sendResult(res, {
                    data: 'Upload successful'
                });
            }
        });
    }, (error) => {
        const httpError = new Meteor.Error('internal-server-error', error);
        httpError.statusCode = 500;
        throw httpError;
    });
```
